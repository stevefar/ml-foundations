import graphlab
products = graphlab.SFrame('amazon_baby.gl/')
products['word_count'] = graphlab.text_analytics.count_words(products['review'])
products['sentiment'] = products['rating'] >=4
selected_words = ['awesome', 'great', 'fantastic', 'amazing', 'love', 'horrible', 'bad', 'terrible', 'awful', 'wow', 'hate']

def t(x):
    arr = {}
    for s in selected_words:
            if s in x:
                arr[s] = x[s]
            else:
                arr[s] = 0
    
    return arr
    
def tt(x, s):
    arr = {}
    if s in x:
        return x[s]
    else:
        return 0

# FOr our model
products["word_count_selected"] = products["word_count"].apply(t);

# Jsut doing this so we can sum
for s in selected_words:
    products[s] = products["word_count"].apply(lambda x: tt(x, s) );
    
train_data,test_data = products.random_split(.8, seed=0)

sentiment_model = graphlab.logistic_classifier.create(train_data,target='sentiment',features=['word_count'],validation_set=test_data)
selected_words_model = graphlab.logistic_classifier.create(train_data,target='sentiment',features=['word_count_selected'],validation_set=test_data)


for s in selected_words:
    print(s, products[s].sum())

# 1.
    # ('awesome', 2090)
    # ('great', 45206) <-- Highest
    # ('fantastic', 932)
    # ('amazing', 1363)
    # ('love', 42065)
    # ('horrible', 734)
    # ('bad', 3724)
    # ('terrible', 748)
    # ('awful', 383)
    # ('wow', 144) <-- Lowest
    # ('hate', 1220)

selected_words_model['coefficients'].sort('value', ascending=False)
selected_words_model['coefficients'].sort('value', ascending=True)
# 2. Love Highest, Horrible lowest

selected_words_model.evaluate(test_data)
# 3. Accuracy 0.7677120261794382
    

    # 140259 / 183531 ... 0.7642251172826389 ... majority class accuracy
    
    # word_count 0.8614125988546496


diaper_champ_reviews = products[products['name'] == 'Baby Trend Diaper Champ']

sentiment_model.evaluate(diaper_champ_reviews)
# 0.93993993993994

sentiment_model.evaluate(diaper_champ_reviews)
# 0.7177177177177178

# 4.

    # "Diaper Champ or Diaper Genie? That was my dilemma when registering for gifts before my son was born. Lucky for me, I had my Mom with me. She runs a daycare and said Diaper Champ is the best by far and that's what I went with. The fact that you can use regular trash bags instead of special cartridges was very appealing to me. Now my son is 6 months old and I must say this thing is GREAT! The only time I catch any smell at all is when I change trash bags. I use those Glad trash bags that are scented to prevent odors and that makes a difference, too. it is very easy and covenient to use and I highly recommend it. The only thing you need to watch is the opening latch is a bit tricky (I broke a nail the first time I used it, but then I got the hang of it). I am glad for this, though, because if it's not easy for me to open, it won't be for my son, either."

    # selecred_words_model 0.710895066562

# selected_words_model = graphlab.logistic_classifier.create(train_data,
#                                                      target='sentiment',
#                                                      features=features,
#                                                      validation_set=test_data)