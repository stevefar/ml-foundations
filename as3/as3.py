import graphlab
people = graphlab.SFrame('people_wiki.gl/')
people['word_count'] = graphlab.text_analytics.count_words(people['text'])
tfidf = graphlab.text_analytics.tf_idf(people['word_count'])
people['tfidf'] = tfidf['docs']

# 1.
elton_john = people[people['name'] == 'Elton John']

elton_john[['word_count']].stack('word_count', new_column_name=['word','word_count']).sort('word_count',ascending=False)
# +-------+------------+
# |  word | word_count |
# +-------+------------+
# |  the  |     27     |
# |   in  |     18     |
# |  and  |     15     |
# |   of  |     13     |
# |   a   |     10     |
# |  has  |     9      |
# |  john |     7      |
# |   he  |     7      |
# |   on  |     6      |
# | award |     5      |
# +-------+------------+

elton_john[['tfidf']].stack('tfidf', new_column_name=['word','tfidf']).sort('tfidf',ascending=False)
# +---------------+---------------+
# |      word     |     tfidf     |
# +---------------+---------------+
# |    furnish    |  18.38947184  |
# |     elton     |  17.48232027  |
# |   billboard   | 17.3036809575 |
# |      john     | 13.9393127924 |
# |  songwriters  |  11.250406447 |
# | tonightcandle | 10.9864953892 |
# |  overallelton | 10.9864953892 |
# |    19702000   | 10.2933482087 |
# |   fivedecade  | 10.2933482087 |
# |      aids     |  10.262846934 |
# +---------------+---------------+


# 2


graphlab.distances.cosine(elton_john['tfidf'][0],people[people['name'] == 'Victoria Beckham']['tfidf'][0])
# 0.9567006376655429

graphlab.distances.cosine(elton_john['tfidf'][0],people[people['name'] == 'Paul McCartney']['tfidf'][0])
# 0.8250310029221779 <- closer to John


# 3

tfidf_model = graphlab.nearest_neighbors.create(people,features=['tfidf'],label='name',distance='cosine')
word_count_model = graphlab.nearest_neighbors.create(people,features=['word_count'],label='name',distance='cosine')

tfidf_model.query(elton_john)
# +-------------+------------------+--------------------+------+
# | query_label | reference_label  |      distance      | rank |
# +-------------+------------------+--------------------+------+
# |      0      |    Elton John    | -2.22044604925e-16 |  1   |
# |      0      |   Rod Stewart    |   0.717219667893   |  2   |
# |      0      |  George Michael  |   0.747600998969   |  3   |
# |      0      | Sting (musician) |   0.747671954431   |  4   |
# |      0      |   Phil Collins   |   0.75119324879    |  5   |
# +-------------+------------------+--------------------+------+

word_count_model.query(elton_john)
# +-------------+-------------------+-------------------+------+
# | query_label |  reference_label  |      distance     | rank |
# +-------------+-------------------+-------------------+------+
# |      0      |     Elton John    | 2.22044604925e-16 |  1   |
# |      0      |   Cliff Richard   |   0.16142415259   |  2   |
# |      0      |   Sandro Petrone  |   0.16822542751   |  3   |
# |      0      |    Rod Stewart    |   0.168327165587  |  4   |
# |      0      | Malachi O'Doherty |   0.177315545979  |  5   |
# +-------------+-------------------+-------------------+------+

tfidf_model.query(people[people['name'] == 'Victoria Beckham'])
# +-------------+---------------------+-------------------+------+
# | query_label |   reference_label   |      distance     | rank |
# +-------------+---------------------+-------------------+------+
# |      0      |   Victoria Beckham  | 1.11022302463e-16 |  1   |
# |      0      |    David Beckham    |   0.548169610263  |  2   |
# |      0      | Stephen Dow Beckham |   0.784986706828  |  3   |
# |      0      |        Mel B        |   0.809585523409  |  4   |
# |      0      |    Caroline Rush    |   0.819826422919  |  5   |
# +-------------+---------------------+-------------------+------+



word_count_model.query(people[people['name'] == 'Victoria Beckham'])
# +-------------+--------------------------+--------------------+------+
# | query_label |     reference_label      |      distance      | rank |
# +-------------+--------------------------+--------------------+------+
# |      0      |     Victoria Beckham     | -2.22044604925e-16 |  1   |
# |      0      | Mary Fitzgerald (artist) |   0.207307036115   |  2   |
# |      0      |      Adrienne Corri      |   0.214509782788   |  3   |
# |      0      |     Beverly Jane Fry     |   0.217466468741   |  4   |
# |      0      |      Raman Mundair       |   0.217695474992   |  5   |
# +-------------+--------------------------+--------------------+------+

