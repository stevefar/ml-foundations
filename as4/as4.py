import graphlab
song_data = graphlab.SFrame('song_data.gl/')

# 1

len(song_data[song_data['artist'] == 'Kanye West']['user_id'].unique())
# 2522

len(song_data[song_data['artist'] == 'Foo Fighters']['user_id'].unique())
# 2055

len(song_data[song_data['artist'] == 'Taylor Swift']['user_id'].unique())
# 3246

len(song_data[song_data['artist'] == 'Lady GaGa']['user_id'].unique())
# 2928

# 2

song_data.groupby(key_columns='artist', operations={'total_count': graphlab.aggregate.SUM('listen_count')}).sort("total_count", ascending=False)
# +------------------------+-------------+
# |         artist         | total_count |
# +------------------------+-------------+
# |     Kings Of Leon      |    43218    |
# |     Dwight Yoakam      |    40619    |
# |         Björk          |    38889    |
# |        Coldplay        |    35362    |
# | Florence + The Machine |    33387    |
# |     Justin Bieber      |    29715    |
# |    Alliance Ethnik     |    26689    |
# |      OneRepublic       |    25754    |
# |         Train          |    25402    |
# |     The Black Keys     |    22184    |
# +------------------------+-------------+


song_data.groupby(key_columns='artist', operations={'total_count': graphlab.aggregate.SUM('listen_count')}).sort("total_count", ascending=True)
# +-------------------------------+-------------+
# |             artist            | total_count |
# +-------------------------------+-------------+
# |        William Tabbert        |      14     |
# |         Reel Feelings         |      24     |
# | Beyoncé feat. Bun B and Sl... |      26     |
# |             Diplo             |      30     |
# |         Boggle Karaoke        |      30     |
# |         harvey summers        |      31     |
# |             Nâdiya            |      36     |
# | Kanye West / Talib Kweli /... |      38     |
# |        Aneta Langerova        |      38     |
# |          Jody Bernal          |      38     |
# +-------------------------------+-------------+


#  3


train_data,test_data = song_data.random_split(.8,seed=0)
personalized_model = graphlab.item_similarity_recommender.create(train_data,user_id='user_id',item_id='song')                                                                
subset_test_users = test_data['user_id'].unique()[0:10000]
recommendations = personalized_model.recommend(subset_test_users,k=1)
recommendations.groupby(key_columns='song', operations={'count': graphlab.aggregate.COUNT()}).sort('count', ascending=False)

# +--------------------------------+-------+
# |              song              | count |
# +--------------------------------+-------+
# |          Undo - Björk          |  431  |
# |     Secrets - OneRepublic      |  385  |
# |    Revelry - Kings Of Leon     |  231  |
# | You're The One - Dwight Yoakam |  170  |
# | Fireflies - Charttraxx Karaoke |  126  |
# |    Hey_ Soul Sister - Train    |  106  |
# | Horn Concerto No. 4 in E f...  |   98  |
# |    Sehr kosmisch - Harmonia    |   69  |
# | OMG - Usher featuring will...  |   59  |
# | Dog Days Are Over (Radio E...  |   55  |
# +--------------------------------+-------+